1. There is a seperate ReadMe.txt file in each folder for better understanding
2. All notebooks are to be ran on google colab
3. Documents to be uploaded on google colab for each notebook are mentioned inside the notebook itself
4. "CNN_IntegratedGradients.ipynb" is my implementation of CNN classifier for classifying case documents which doesn't work
5. "CNN_Authours.ipynb" is authors python code from their repo which doesn't work either. Even without converting their python script files into notebook, it doesn't work. They have missing files and outdated sentence encoder
6. Run the Files in following order: "CasePreprocessing.ipynb" -> "CNN_xxxx.ipynb"(both don't work) -> "TypeClassifier.ipynb" -> "Summarizing.ipynb"
7. "annotated_casetext", "Sents For Summary" and "attribution.zip" contains documents taken from authors repo
8. GitHub repo of authors code: https://github.com/luimagroup/bva-summarization


1. This folder contains annotated cases.
2. Each sentence in a case is annotated into a given class.
3. "All" folder contains complete dataset
4. "Train", "Test" and "Validation" folders contain randomly sampled cases from "All" folder

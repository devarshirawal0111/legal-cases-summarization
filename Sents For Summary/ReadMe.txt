1. "data" folder contains extracted predictive sentences from CNN classifier used by authors.
2. "ref_summaries" folder contains reference summaries obtained from their github repo.
3. "AllSumCases.txt" contains list of cases that are also present in "annotated_casetext" folder in root directory.
4. "lr_model.pickle" is pickle file of logistic regression model used to classify sentencs into imp_set and others_set.
5. .zip files are compressed folders which can be used to upload on google colab.
